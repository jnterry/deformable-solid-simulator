package com.jamieterry.simulation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;

import com.jamieterry.graphics.Drawable;
import com.jamieterry.math.Vector2d;

public class AABB implements Drawable{
	protected double mXMax;
	protected double mXMin;
	protected double mYMax;
	protected double mYMin;
	
	public void transform(Vector2d vec){
		mXMax += vec.getX();
		mXMin += vec.getX();
		mYMax += vec.getY();
		mYMin += vec.getY();
	}
	
	@Override
	public void draw(Graphics2D g2d) {
		g2d.setStroke(new BasicStroke(3));
		g2d.setColor(Color.RED);
		g2d.drawRect((int)(mXMin), (int)(mYMin), (int)(mXMax-mXMin), (int)(mYMax-mYMin));
		g2d.setStroke(new BasicStroke(1));
	}
	
	//Constructs an AABB from a collection of points
	public AABB(Collection<Vector2d> points){
		mXMax = -100000;
		mYMax = -100000;
		
		mXMin = 1000000;
		mYMin = 1000000;
		
		for(Vector2d v : points){
			this.addPoint(v);
		}
	}
	
	public AABB(){
		mXMax = -100000;
		mYMax = -100000;
		
		mXMin = 1000000;
		mYMin = 1000000;
	}
	
	public AABB(AABB aabbToCopy) {
		this.mXMax = aabbToCopy.mXMax;
		this.mYMax = aabbToCopy.mYMax;
		this.mXMin = aabbToCopy.mXMin;
		this.mYMin = aabbToCopy.mYMin;
	}

	public void addPoint(Vector2d point){
		if(point.getX() > mXMax){
			mXMax = point.getX();
		} else if (point.getX() < mXMin){
			mXMin = point.getX();
		}
		
		if(point.getY() > mYMax){
			mYMax = point.getY();
		} else if (point.getY() < mYMin){
			mYMin = point.getY();
		}
	}
	
	public void expandBox(double size){
		mXMax += size;
		mYMax += size;
		
		mXMin -= size;
		mYMin -= size;
	}
	
	public boolean isCollidingWith(AABB otherAABB){
		//http://www.miguelcasillas.com/?p=30
		
		return(this.mXMax > otherAABB.mXMin && 
			    this.mXMin < otherAABB.mXMax &&
			    this.mYMax > otherAABB.mYMin &&
			    this.mYMin < otherAABB.mYMax);
	}
	
	public boolean isCollidingWith(Vector2d point){
		return (point.getX() < this.mXMax &&
				point.getX() > this.mXMin &&
				point.getY() < this.mYMax &&
				point.getY() > this.mYMin);
	}
	
	
}
