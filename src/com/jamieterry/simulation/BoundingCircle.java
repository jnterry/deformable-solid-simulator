package com.jamieterry.simulation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;

import com.jamieterry.graphics.Drawable;
import com.jamieterry.math.Vector2d;

public class BoundingCircle implements Drawable{
	Vector2d mCenter;
	double mRadiusSquared;
	double mRadius;
	
	//construct bounding sphere from points
	BoundingCircle(Collection<Vector2d> points){
		/////////////////////////////////////////////////////////////////////////////
		//first find centre of all points, this is just the average of all the points
		long totalX = 0;
		long totalY = 0;
		for(Vector2d v : points){
			totalX += v.getX();
			totalY += v.getY();
		}
		//divide total by number of points to find average
		mCenter = new Vector2d(totalX/points.size(), totalY/points.size());
		
		/////////////////////////////////////////////////////////////////////////////
		//now find farthest out point
		mRadiusSquared = -1;
		for(Vector2d v : points){
			double distance = v.getDistanceToSquared(mCenter);
			if(distance > mRadiusSquared){
				mRadiusSquared = distance;
			}
		}
		mRadius = Math.sqrt(mRadiusSquared);
	}
	
	public BoundingCircle(BoundingCircle bcToCopy) {
		this.mCenter = new Vector2d(bcToCopy.mCenter);
		this.mRadiusSquared = bcToCopy.mRadiusSquared;
		this.mRadius = bcToCopy.mRadius;
		
	}

	public boolean isCollidingWith(BoundingCircle otherSphere){
		double centerDistanceSquared = mCenter.getDistanceToSquared(otherSphere.mCenter)/4;
		return (centerDistanceSquared < mRadiusSquared || centerDistanceSquared < otherSphere.mRadiusSquared);
	}
	
	public boolean isCollidingWith(Vector2d point){
		return (mCenter.getDistanceToSquared(point) < this.mRadiusSquared);
	}

	@Override
	public void draw(Graphics2D g2d) {
		g2d.setStroke(new BasicStroke(1));
		g2d.setColor(Color.RED);
		g2d.drawLine((int)(mCenter.getX()-mRadius), (int)(mCenter.getY()), (int)(mCenter.getX()+mRadius), (int)(mCenter.getY()));
		g2d.drawLine((int)(mCenter.getX()), (int)(mCenter.getY()-mRadius), (int)(mCenter.getX()), (int)(mCenter.getY()+mRadius));
		g2d.drawOval((int)(mCenter.getX()-mRadius), (int)(mCenter.getY()-mRadius), (int)(mRadius*2), (int)(mRadius*2));
		g2d.setStroke(new BasicStroke(1));
	}
	
	public void expand(double size){
		this.mRadius += size;
		this.mRadiusSquared = this.mRadius*this.mRadius;
	}
	
	public void transform(Vector2d vec){
		this.mCenter.addEquals(vec);
	}
}
