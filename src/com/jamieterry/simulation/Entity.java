package com.jamieterry.simulation;

import java.awt.Graphics2D;

import com.jamieterry.graphics.Drawable;
import com.jamieterry.math.Vector2d;

public abstract class Entity implements Drawable{
	protected CollisionInfo mCollisionInfo; 
	
	protected EntityScene mScene;
	
	public abstract void update(double deltaTime);
	public abstract void draw(Graphics2D g2d);
	
	public abstract void setPosition(Vector2d newPos);
	public abstract Vector2d getPosition();
	
	//public abstract void setVelocity(Vector2d newVel);
	//public abstract Vector2d getVelocity();
	
	public abstract void checkColldide(Entity otherEnt, double deltaTime);
	
	public void deleteEntity(){
		this.mScene.removeEntity(this);
		this.mScene = null;
	}
	
	
}
