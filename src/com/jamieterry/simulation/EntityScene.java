package com.jamieterry.simulation;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.jamieterry.graphics.Drawable;
import com.jamieterry.math.Vector2d;

public class EntityScene implements Drawable{
	
	Entity mActiveEntity;
	
	long mTimeForLastUpdate;
	int mUpdatesPerSecond = 0;
	Thread mUpdaterThread;
	ReentrantLock mThreadLock;
	
	int mWidth;
	int mHeight;
	
	boolean mDrawDebugInfo = false;

	
	ArrayList<Entity> mEntities;
	
	double mDeltaTime = 0.000001;
	
	public void setActiveEntity(Entity e){
		this.mActiveEntity = e;
	}
	
	public Entity getActiveEntity(){
		return this.mActiveEntity;
	}
	
	public void setDrawDebugInfo(boolean draw){
		this.mDrawDebugInfo = draw;
	}
	
	public EntityScene(){
		mEntities = new ArrayList<Entity>();
		mThreadLock = new ReentrantLock();
	}
	
	public void setSize(int width, int height){
		this.mWidth = width;
		this.mHeight = height;
	}
	
	public void addEntity(Entity e){//to scene
		this.mThreadLock.lock();
		mEntities.add(e);
		e.mScene = this;
		this.mThreadLock.unlock();
	}
	
	public void removeEntity(Entity e){//from scene
		this.mThreadLock.lock();
		mEntities.remove(e);
		e.mScene = null;
		
		if(e == this.mActiveEntity){
			this.mActiveEntity = null;
		}
		
		this.mThreadLock.unlock();
	}
	
	public void clearScene(){
		this.mThreadLock.lock();
		mEntities = new ArrayList<Entity>();
		this.mThreadLock.unlock();
	}
	
	public void setDeltaTimeModifier(double newModifier){
		this.mThreadLock.lock();
		this.mDeltaTime = 0.000001 * newModifier;
		this.mThreadLock.unlock();
	}
	
	public void updateScene(){
		this.mThreadLock.lock();
				
		////////////////////////////////////////////////////////////////////////////
		//Update all entities
		for(Entity e : mEntities){
			e.update(this.mDeltaTime);
		}
		
		////////////////////////////////////////////////////////////////////////////
		//Check for collisions between all entities
		for(Entity e1 : mEntities){
			for (Entity e2: mEntities){
				if(e1 != e2){ //make sure entities don't check for collision with themselves!
					//if(e1.mCollisionInfo.isCollidingWith(e2.mCollisionInfo)){
						e1.checkColldide(e2, this.mDeltaTime);
					//}
				}
			}
		}
		
		////////////////////////////////////////////////////////////////////////////
		//Update position of entities based of velocity (which may have been changed by .onCollide
		/*for(Entity e : mEntities){
			e.transform(Vector2d.multiply(e.mVelocity, mDeltaTime));
		}*/
		this.mThreadLock.unlock();
	}
	
	@Override
	public void draw(Graphics2D g2d) {
		
		
		int entCount, bondCount = 0, particleCount = 0;
		entCount = mEntities.size();
		
		for(Entity e : mEntities){
			//draw all entities
			e.draw(g2d);
			
			//while looping, may as well count up bonds and particle 
			if (e instanceof DeformableMeshEntity){
				bondCount += ((DeformableMeshEntity) e).getBondCount();
				particleCount += ((DeformableMeshEntity) e).getParticleCount();
			}
		}
		
		//draw the bounding boxes on the active entity
		if(mActiveEntity != null){
			this.mActiveEntity.mCollisionInfo.mAABB.draw(g2d);
		}
		
		if(this.mDrawDebugInfo){
			g2d.setColor(Color.BLACK);
			g2d.drawString("Physics Updates per second: " + this.mUpdatesPerSecond, 10, 40 );
			g2d.drawString(entCount + " Entities (" + particleCount + " particles, " + bondCount + " bonds)"
					, 10, 60 );
		}
	}//end of draw


	public void beginUpdateThread() {
		if(mUpdaterThread == null){
			mUpdaterThread = new Thread(new mUpdaterRunnable());
		}
		this.mUpdaterThread.start();
	}
	
	public Entity getEntityAtPoint(Vector2d point){
		for(Entity e : this.mEntities){
			if(e.mCollisionInfo.isCollidingWith(point)){
				return e;
			}
		}
		
		//if still going then no entity was at point, return null
		return null;
	}
	
	protected class mUpdaterRunnable implements Runnable{

		@Override
		public void run() {
			long startTime = 0;
			long timer = 0;
			int updateCounter = 0;
			while(true){
				startTime = System.nanoTime();
				updateScene();
				mTimeForLastUpdate = System.nanoTime()-startTime;
				timer+=mTimeForLastUpdate;
				++updateCounter;
				if(timer > 500000000){//greater than 1/2 a secondv
					mUpdatesPerSecond = updateCounter*2;
					timer = 0;
					updateCounter = 0;
				}
				
			}
		}
	}
}
