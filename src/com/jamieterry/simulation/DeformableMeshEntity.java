package com.jamieterry.simulation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;

import com.jamieterry.math.Vector2d;

public abstract class DeformableMeshEntity extends Entity{
	
	static boolean msDrawParticleVelocity = false;
	static boolean msDrawBonds = false;
	static boolean msDarkenLowVelParticles = false;
	
	protected Vector2d mGravity;
	
	protected double mParticleRadius = 6;
	protected double mBondStiffness = 1;
	protected double mEnergyLossPercentage = 1;
	
	protected Color mColor;
	
	protected boolean mIsStatic;

	protected ArrayList<Particle> mParticles;
	protected ArrayList<Particle> mOuterParticles;
	protected ArrayList<Bond> mBonds;
	
	
	
	protected DeformableMeshEntity(){
		mParticles = new ArrayList<Particle>();
		mOuterParticles = new ArrayList<Particle>();
		mColor = Color.GRAY;
		mGravity = new Vector2d(0,10000);
		mIsStatic = false;
	}
	
	public int getBondCount(){
		return this.mBonds.size();
	}
	
	public void setStatic(boolean isStatic){
		this.mIsStatic = isStatic;
		
	}
	public int getParticleCount(){
		return this.mParticles.size();
	}
	
	public void setColor(Color c){
		mColor = c;
	}
	
	public void setBondStiffness(double newStiffness){
		this.mBondStiffness = newStiffness;
	}
	
	public double getBondStiffness(){
		return this.mBondStiffness;
	}
	
	public void setPosition(Vector2d newPos){
		//find the offset between current and desired position, then transform by that amount
		this.transformPosition(Vector2d.subtract(newPos, this.getPosition()));
	}
	
	public Vector2d getPosition(){
		return this.mCollisionInfo.mBoundingCircle.mCenter;
	}
	
	public void transformPosition(Vector2d offset){
		for(Particle p : this.mParticles){
			p.position.addEquals(offset);
		}
		this.mCollisionInfo.transform(offset);
	}
	
	public boolean setEnergyLosePercentage(double loss){
		if(loss >= 0 && loss <= 100){
			this.mEnergyLossPercentage = loss;
			return true;
		} else {
			return false;
		}
	}
	
	public double getEnergyLossPercentage(){
		return this.mEnergyLossPercentage;
	}
	//@Override
	//public void setVelocity(Vector2d newVel) {
	//	this.mSharedVelocity = newVel;
	//}

	//@Override
	//public Vector2d getVelocity() {
	//	return mSharedVelocity;
	//}
	
	public void setGravity(Vector2d newGravity){
		this.mGravity = newGravity;
	}
	
	
	
	
	@Override
	public void draw(Graphics2D g2d) {
		if(this.msDarkenLowVelParticles){
			double totalVelocityX = 0;
			double totalVelocityY = 0;
			for(Particle p : mParticles){
				totalVelocityX += p.velocity.getX();
				totalVelocityY += p.velocity.getY();
			}
			totalVelocityX /= mParticles.size();
			totalVelocityY /= mParticles.size();
			double averageVelocity = new Vector2d(totalVelocityX, totalVelocityY).getLength();
			
			for(Particle p : mParticles){
				//draw particle
				int colorPercent = (int) (p.velocity.getLength() / averageVelocity);
				if(colorPercent == 0){
					colorPercent = 1;
				}
				g2d.setColor(new Color(mColor.getRed()/(colorPercent), mColor.getGreen()/(colorPercent), mColor.getBlue()/(colorPercent)));
				g2d.fillOval(	(int) (p.position.getX() - mParticleRadius),
								(int) (p.position.getY() - mParticleRadius),
								(int) mParticleRadius*2,
								(int) mParticleRadius*2);
			}
		} else { //dont darken low velocity particles
			for(Particle p : mParticles){
				//draw particle
				g2d.setColor(this.mColor);
				g2d.fillOval(	(int) (p.position.getX() - mParticleRadius),
								(int) (p.position.getY() - mParticleRadius),
								(int) mParticleRadius*2,
								(int) mParticleRadius*2);
			}
		}
			

			
		//draw particle velocity
		if(msDrawParticleVelocity){
			for(Particle p : this.mParticles){
				g2d.setColor(Color.GREEN);
				g2d.drawLine(	(int) (p.position.getX()), 
								(int) (p.position.getY()),
								(int) (p.position.getX() + (p.velocity.getX()/80)),
								(int) (p.position.getY() + (p.velocity.getY()/80)));
			}
		}
		
		if(msDrawBonds){
			for(Bond b : this.mBonds){
				//draw current bond
				g2d.setColor(Color.BLACK);
				g2d.drawLine(	(int) (b.particleA.position.getX()), 
								(int) (b.particleA.position.getY()),
								(int) (b.particleB.position.getX()), 
								(int) (b.particleB.position.getY()));
				
			}
		}
		
		//draw deformableMeshEntity centre, AABB, etc
		//if(this.mCollisionInfo != null){
		//	this.mCollisionInfo.draw(g2d);
		//}
		
	}
	
	public void setGlobalVelocity(Vector2d newVelocity){
		for(Particle p : this.mParticles){
			p.velocity = new Vector2d(newVelocity);
		}
	}
	
	public static void setDrawParticleVelocity(boolean draw){
		msDrawParticleVelocity = draw;
	}
	
	public static void setDrawBonds(boolean draw){
		msDrawBonds = draw;
	}
	
	public static void setDarkenLowVelParticles(boolean draw){
		msDarkenLowVelParticles = draw;
	}
	
	public Vector2d getGravity(){
		return this.mGravity;
	}
	
	
	
	public void update(double deltaTime) {
			
		for(Bond b : this.mBonds){
			//find percentage change in bond length
			double currentDistance = (b.particleA.position.getDistanceTo(b.particleB.position));
			double percentageChange = (currentDistance-b.idealLength)/b.idealLength;
			
			//get vector from A to B
			Vector2d AToB = Vector2d.subtract(b.particleA.position, b.particleB.position);
			AToB.multiplyEquals(-percentageChange*10000000*deltaTime*mBondStiffness);
			b.particleA.velocity.addEquals(AToB);
				
			Vector2d BToA = Vector2d.subtract(b.particleB.position, b.particleA.position);
			BToA.multiplyEquals(-percentageChange*10000000*deltaTime*mBondStiffness);
			b.particleB.velocity.addEquals(BToA);
		}
		
		if(! this.mIsStatic){
			//then check for collision with edge of scene, if close to edge, move ball away
			int edgeRepulsionRadius = 10;
			//check with bottom
			for(Particle p : this.mParticles){
				
				//check bottom
				if(mScene.mHeight - p.position.getY() < edgeRepulsionRadius){
					//do 1 / distance from bottom squared
					double force = 1/Math.pow(mScene.mHeight - p.position.getY(),2);
					p.velocity.addEquals(new Vector2d(0,-force*100000*Math.sqrt(deltaTime)));
				}
				
				//check right
				if(p.position.getX() < mScene.mWidth + edgeRepulsionRadius){
					double force = 1/Math.pow(mScene.mWidth - p.position.getX(),2);
					p.velocity.addEquals(new Vector2d(-force*100000*Math.sqrt(deltaTime), 0));
				}
				
				//check left
				if(p.position.getX() < edgeRepulsionRadius){
					double force = 1/Math.pow(-p.position.getX(),2);
					p.velocity.addEquals(new Vector2d(force*100000*Math.sqrt(deltaTime), 0));
				}
				
				//check top
				if(p.position.getY() < edgeRepulsionRadius){
					double force = 1/Math.pow(-p.position.getY(),2);
					p.velocity.addEquals(new Vector2d(0, force*100000*Math.sqrt(deltaTime)));
				}
			}
		}
		for(Particle p : this.mParticles){
			p.velocity.addEquals(Vector2d.multiply(mGravity,deltaTime));
			p.position.addEquals(Vector2d.multiply(p.velocity,deltaTime));
			
			p.velocity.multiplyEquals(1 - mEnergyLossPercentage/10*deltaTime);
		}
		
		this.updateCollisionInfo();	
	}
	
	//creates a two way bond between two particles
	protected void bondParticles(Particle p1, Particle p2){
		
		//check the bond doesn't already exist
		for(Bond b : this.mBonds){
			if( (b.particleA == p1 && b.particleB == p2) ||
				(b.particleB == p1 && b.particleA == p2)){
				return;
			}
		}
		
		//otherwise (if haven't yet returned) bond does not exist, make it		
		Bond bond = new Bond();
		bond.idealLength = p1.position.getDistanceTo(p2.position);
		bond.particleA = p1;
		bond.particleB = p2;
		this.mBonds.add(bond);

		
	}
	
	public void updateCollisionInfo(){
		ArrayList<Vector2d> points = new ArrayList<Vector2d>();
		for(Particle p : this.mOuterParticles){
			points.add(p.position);
		}
		this.mCollisionInfo = new CollisionInfo(points);
		this.mCollisionInfo.expand(this.mParticleRadius);
	}
	
	@Override
	public void checkColldide(Entity otherEnt, double deltaTime) {
		if(otherEnt instanceof DeformableMeshEntity){
			DeformableMeshEntity e = (DeformableMeshEntity) otherEnt;
			for(Particle myPart : this.mOuterParticles){
				for(Particle otherPart : e.mOuterParticles){
					double particleDistanceSquared = myPart.position.getDistanceToSquared(otherPart.position);
					if(particleDistanceSquared < 300){
						myPart.velocity.addEquals(Vector2d.multiply(Vector2d.subtract(myPart.position,  otherPart.position), particleDistanceSquared*deltaTime*100000));
					}
				}
			}
		}
	}
	
	public class Particle {
		public Vector2d position;
		public Vector2d velocity;
		
		public Particle(){
			position = new Vector2d();
			velocity = new Vector2d();
			mBonds = new ArrayList<Bond>();
		}
		
		public Particle(Vector2d pos){
			position = pos;
			velocity = new Vector2d();
			mBonds = new ArrayList<Bond>();
		}
	}
	
	public class Bond{
		public double idealLength;
		public Particle particleA;
		public Particle particleB;
	}
}
