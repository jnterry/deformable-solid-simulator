package com.jamieterry.simulation;

import java.awt.Graphics2D;
import java.util.Collection;

import com.jamieterry.graphics.Drawable;
import com.jamieterry.math.Vector2d;

public class CollisionInfo implements Drawable{
	AABB mAABB;
	BoundingCircle mBoundingCircle;
	
	CollisionInfo(Collection<Vector2d> points){
		mAABB = new AABB(points);
		mBoundingCircle = new BoundingCircle(points);
	}
	
	CollisionInfo(CollisionInfo cInfoToCopy){
		mAABB = new AABB(cInfoToCopy.mAABB);
		mBoundingCircle = new BoundingCircle(cInfoToCopy.mBoundingCircle);
	}
	
	public void transform(Vector2d vec){
		mAABB.transform(vec);
		mBoundingCircle.transform(vec);
	}
	
	
	public boolean isCollidingWith(CollisionInfo otherInfo){
		return (mAABB.isCollidingWith(otherInfo.mAABB) && 
				mBoundingCircle.isCollidingWith(otherInfo.mBoundingCircle));
	}
	public boolean isCollidingWith(Vector2d point){
		return (this.mAABB.isCollidingWith(point) &&
				this.mBoundingCircle.isCollidingWith(point));
	}
	@Override
	public void draw(Graphics2D g2d) {
		mAABB.draw(g2d);
		mBoundingCircle.draw(g2d);
	}
	
	public void expand(double size){
		mAABB.expandBox(size);
		mBoundingCircle.expand(size);
	}
	
	public Vector2d getCollisionLocation(){
		Vector2d result = new Vector2d();
		return result;
	}
}
