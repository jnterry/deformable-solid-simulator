package com.jamieterry.simulation;

import com.jamieterry.math.Vector2d;

public class Particle {
	Vector2d position;
	Vector2d force;
	
	Particle(){
		position = new Vector2d();
		force = new Vector2d();
	}
}
