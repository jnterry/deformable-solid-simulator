package com.jamieterry.math;

public class Vector2d{
	private double mX, mY;
	
	////////////////////////////////////////////////////////////////////////
	//Setters and getters
	public double getX(){return mX;}
	public double getY(){return mY;}
	public void set(double newX, double newY){mX = newX; mY = newY;}
	public void setX(double newX){mX = newX;}
	public void setY(double newY){mY = newY;}
	
	////////////////////////////////////////////////////////////////////////
	//Constructors
	public Vector2d(){
		mX = 0;
		mY = 0;
	}
	//copys values from other vector
	public Vector2d(Vector2d otherVector){
		this.mX = otherVector.mX;
		this.mY = otherVector.mY;
	}
	
	public Vector2d(double x, double y){
		this.mX = x;
		this.mY = y;
	}
	
	////////////////////////////////////////////////////////////////////////
	//Basic Vector operators
	public Vector2d addEquals(Vector2d otherVector){ //this += other
		this.mX += otherVector.mX;
		this.mY += otherVector.mY;
		return this;
	}
	
	public static Vector2d add(Vector2d vectorA, Vector2d vectorB){ // Vector = A + B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.addEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d subtractEquals(Vector2d otherVector){ //this -= other
		this.mX -= otherVector.mX;
		this.mY -= otherVector.mY;
		return this;
	}
	
	public static Vector2d subtract(Vector2d vectorA, Vector2d vectorB){ // Vector = A - B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.subtractEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d divideEquals(Vector2d otherVector){ //this /= other
		this.mX /= otherVector.mX;
		this.mY /= otherVector.mY;
		return this;
	}
	
	public static Vector2d divide(Vector2d vectorA, Vector2d vectorB){ // Vector = A / B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.divideEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d multiplyEquals(Vector2d otherVector){ //this *= other
		this.mX *= otherVector.mX;
		this.mY *= otherVector.mY;
		return this;
	}
	
	public static Vector2d multiply(Vector2d vectorA, Vector2d vectorB){ // Vector = A * B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.multiplyEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d multiplyEquals(double magnitudeModifier){
		this.mX *= magnitudeModifier;
		this.mY *= magnitudeModifier;
		return this;
	}
	
	public static Vector2d multiply(Vector2d vec, double magnitudeModifier){ // Vector = A * B
		Vector2d result = new Vector2d(vec);//copy vector a data into result
		result.multiplyEquals(magnitudeModifier); //now add a and b, using += code
		return result; //return result
	}
	
	
	
	
	////////////////////////////////////////////////////////////////////////
	//Other vector operators
	public double getLength(){
		//get distance between this vector and (0,0)
		return this.getDistanceTo(new Vector2d(0,0));
	}
	
	public double getLengthSquared(){
		return this.getDistanceToSquared(new Vector2d(0,0));
	}
	
	public double getDistanceTo(Vector2d otherVector){
		return Math.sqrt(getDistanceToSquared(otherVector));
	}
	
	public double getDistanceToSquared(Vector2d otherVector){
		//distance squared = (deltaX^2)+(deltaY^2)
		return ((this.mX - otherVector.mX)*(this.mX - otherVector.mX)) + ((this.mY - otherVector.mY)*(this.mY - otherVector.mY));
	}
	
	public double getAngleTo(Vector2d otherVector){
		double result = Math.atan2(otherVector.mY - this.mY, otherVector.mX - this.mX);
		if (result < 0){
			result += Math.PI;
		}
		return result;
	}
	
	public Vector2d reverseDirection(){ //inverts the vector
		mX = - mX;
		mY = - mY;
		return this;
	}
	
	public static Vector2d createVectorFromAngleAndMagnitude(double angle, double magnitude){
		return new Vector2d(magnitude*Math.cos(angle), magnitude*Math.sin(angle));
	}
	public void set(Vector2d otherVector) {
		this.mX = otherVector.mX;
		this.mY = otherVector.mY;
		
	}
	
	
	
	
	
	
	
	
}
