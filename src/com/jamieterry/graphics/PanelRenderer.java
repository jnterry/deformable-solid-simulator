package com.jamieterry.graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.EntityScene;

public class PanelRenderer extends JPanel{
	
	boolean mDrawFrameRate;
	EntityScene mScene;
	
	long mTimer = 0;
	int mFrameCounter = 0;
	int mFrameRate = 0;
	
	public PanelRenderer(EntityScene scene){
		mScene = scene;
	}
	
	public PanelRenderer(){
		mScene = null;
	}
	
	public void setDisplayFramerate(boolean draw){this.mDrawFrameRate = draw;}
	
	@Override
    public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		
		if(System.nanoTime() - mTimer > 500000000){
			this.mFrameRate = mFrameCounter*2;
			mFrameCounter = 0;
			mTimer = System.nanoTime();
		}
			
		if(this.mDrawFrameRate){
			g2d.drawString("Frame Rate: " + this.mFrameRate, 10, 20);
		}
		
		if(mScene != null){
	        mScene.draw(g2d);
		}
		
		++this.mFrameCounter;
        
    }

}
