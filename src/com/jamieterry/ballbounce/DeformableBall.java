package com.jamieterry.ballbounce;

import java.util.ArrayList;

import com.jamieterry.graphics.PanelRenderer;
import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.CollisionInfo;
import com.jamieterry.simulation.DeformableMeshEntity;
import com.jamieterry.simulation.Entity;

public class DeformableBall extends DeformableMeshEntity{

	DeformableBall(final Vector2d entPos, double particleRadius, double rings, double spacing, double bondCountModifier) {
		super();
		this.mParticleRadius = particleRadius;
		double totalSpaceOffset = spacing + this.mParticleRadius*2;
		Particle centPart = new Particle(entPos);
		this.mParticles.add(centPart);
		if(rings == 1){
			//then the center is the a outer particle
			this.mOuterParticles.add(centPart);
		}
		for(int curRing = 1; curRing < rings; ++curRing){
			int circleCount = (int)((2*Math.PI*curRing*totalSpaceOffset)/totalSpaceOffset); //circumference / totalSpaceOffset
			for(int i = 0; i < circleCount; i++){
				Particle p = new Particle();
				p.position = Vector2d.add(entPos, Vector2d.createVectorFromAngleAndMagnitude((Math.PI*i*2)/circleCount, curRing*totalSpaceOffset));
				this.mParticles.add(p);
				if(curRing >= rings-totalSpaceOffset){
					this.mOuterParticles.add(p);
				}
			}
		}
		//System.out.println("Made " + mParticles.size() + " particles!");
		
		
		/*int circleIndex = 0;
		for(double curRadius = 1; curRadius < radius; curRadius+=totalSpaceOffset){
			int circleCount = (int)((2*Math.PI*curRadius)/totalSpaceOffset);//circumference / totalSpaceOffset
			int oldCircleCount = circleCount;
			//System.out.println(circleCount);
			for(int i = 0; i < circleCount; i++){
				circleIndex++;
				
				try{
					this.bondParticles(mParticles.get(circleIndex), mParticles.get(circleIndex+1));
					this.bondParticles(mParticles.get(circleIndex), mParticles.get(circleIndex-oldCircleCount));
					this.bondParticles(mParticles.get(circleIndex), mParticles.get(circleIndex+oldCircleCount));
				} catch (IndexOutOfBoundsException e){
					
				}
			}
		}*/
		
		//now set up bonds
		//for each particle, find all particles within a certain distance, then bond them
		//the certain distance is the minimum distance between particles plus a small amount
		//first, find minimum distance
		double minParticleDistance = 10000000;
		for(Particle p1 : this.mParticles){
			for(Particle p2 : this.mParticles){
				if(p1 != p2){
					if(p1.position.getDistanceToSquared(p2.position) < minParticleDistance){
						minParticleDistance = p1.position.getDistanceToSquared(p2.position);
					}
				}
			}
		}
		
		for(Particle p1 : this.mParticles){
			for(Particle p2 : this.mParticles){
				if(p1 != p2){
					if(p1.position.getDistanceToSquared(p2.position) < minParticleDistance*bondCountModifier){
						this.bondParticles(p1, p2);
					}
				}
			}
		}
		
		updateCollisionInfo();
		
	}



	

}
