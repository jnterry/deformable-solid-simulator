package com.jamieterry.ballbounce;

import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.DeformableMeshEntity;

public class DeformableRectangle extends DeformableMeshEntity{
	
	DeformableRectangle(Vector2d entPos, int rows, int columns, double particleRadius, double padding){
		double totalSize = particleRadius*2+padding;
		Particle[][] partArray = new Particle[rows][columns];
		
		for(int r = 0; r < rows; ++r){
			for(int c = 0; c < columns; ++c){
				partArray[r][c] = new Particle(new Vector2d(entPos.getX()+totalSize*c, entPos.getY()+totalSize*r));
			}
		}
		
		//set up bonds
		/*for(int r = 0; r < rows; ++r){
			for(int c = 0; c < columns-1; ++c){
				this.bondParticles(partArray[r][c], partArray[r][c+1]);
			}
		}
		for(int r = 0; r < rows-1; ++r){
			for(int c = 0; c < columns; ++c){
				this.bondParticles(partArray[r][c], partArray[r+1][c]);
			}
		}*/
		
		//now transfer the particles from the array to the arraylist
		for(int r = 0; r < rows; ++r){
			for(int c = 0; c < columns; ++c){
				this.mParticles.add(partArray[r][c]);
			}
		}
		
		for(int i = 0; i < this.mParticles.size(); ++i){
			for(int j = 0; j < this.mParticles.size(); j++){
				if(i != j){
					if(this.mParticles.get(i).position.getDistanceTo(this.mParticles.get(j).position) < totalSize*1.6){
						this.bondParticles(this.mParticles.get(i), this.mParticles.get(j));
					}
				}
			}
		}
		
		//populate the outer particles list
		for(int r = 0; r < rows; ++r){
			this.mOuterParticles.add(partArray[r][0]);
			this.mOuterParticles.add(partArray[r][columns-1]);
		}
		for(int c = 1; c < columns-1; ++c){
			this.mOuterParticles.add(partArray[0][c]);
			this.mOuterParticles.add(partArray[rows-1][c]);
		}
		
		System.out.println(this.mParticles.size() + " particles, " + this.mBonds.size() + " bonds.");
		/*partArray[0][48].position.addEquals(new Vector2d(0,50));
		partArray[0][49].position.addEquals(new Vector2d(0,100));
		partArray[0][50].position.addEquals(new Vector2d(0,150));
		partArray[0][51].position.addEquals(new Vector2d(0,200));
		partArray[0][52].position.addEquals(new Vector2d(0,150));
		partArray[0][53].position.addEquals(new Vector2d(0,100));
		partArray[0][54].position.addEquals(new Vector2d(0,50));*/
		
		
		
		
		//updateCollisionInfo();
		
	}


	
	
}
