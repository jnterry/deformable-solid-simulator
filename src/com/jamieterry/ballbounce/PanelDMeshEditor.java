package com.jamieterry.ballbounce;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.DeformableMeshEntity;

public class PanelDMeshEditor extends JPanel implements ActionListener{
	
	DeformableMeshEntity mEntity;
	
	JTextField mBondStiffnessField = new JTextField(7);
	JButton mDeleteEntityButton = new JButton("Delete Entity");
	
	JTextField mResetVelXBox = new JTextField(7);
	JTextField mResetVelYBox = new JTextField(7);
	
	JButton mColRedButton = new JButton(" ");
	JButton mColGreenButton = new JButton(" ");
	JButton mColYellowButton = new JButton(" ");
	JButton mColBlueButton = new JButton(" ");
	JButton mColGrayButton = new JButton(" ");
	JButton mColPinkButton = new JButton(" ");
	
	JTextField mGravityXBox = new JTextField();
	JTextField mGravityYBox = new JTextField();
	
	JTextField mEnergyLossBox = new JTextField();
	void setEntity(DeformableMeshEntity e){
		mEntity = e;
		this.createPanel();
	}
	
	PanelDMeshEditor(){
		
		this.setLayout(new MigLayout());
		this.setMinimumSize(new Dimension(250,0));
		this.setMaximumSize(new Dimension(250,100000000));
		
		this.mDeleteEntityButton.addActionListener(this);
		
		this.mBondStiffnessField.addActionListener(this);
		
		
		this.mResetVelXBox.addActionListener(this);
		this.mResetVelYBox.addActionListener(this);
		
		this.mGravityXBox.addActionListener(this);
		this.mGravityYBox.addActionListener(this);
		
		this.mEnergyLossBox.addActionListener(this);
		
		mColRedButton.setBackground(Color.RED);
		mColGreenButton.setBackground(Color.GREEN);
		mColYellowButton.setBackground(Color.YELLOW);
		mColBlueButton.setBackground(Color.BLUE);
		mColGrayButton.setBackground(Color.GRAY);
		mColPinkButton.setBackground(Color.PINK);
		mColRedButton.addActionListener(this);
		mColGreenButton.addActionListener(this);
		mColYellowButton.addActionListener(this);
		mColBlueButton.addActionListener(this);
		mColGrayButton.addActionListener(this);
		mColPinkButton.addActionListener(this);
	}
	
	void createPanel(){
		//1st remove everything
		this.removeAll();
		if(mEntity != null){
			
			
			this.setLayout(new MigLayout());
			this.setMinimumSize(new Dimension(250,0));
			this.setMaximumSize(new Dimension(250,100000000));
			
			this.add(new JLabel("Entity Infomation: "), "span");
			this.add(new JLabel(mEntity.getParticleCount() + " particles, " + mEntity.getBondCount() + " bonds"), "span, wrap");
			
			this.add(this.mDeleteEntityButton, "growx, span, wrap");
			
			this.add(new JLabel("Reset Particle Velcoity:"), "span");
			this.add(new JLabel ("X: "), "split");
			this.add(this.mResetVelXBox, "growx, split");
			this.add(new JLabel ("Y: "), "split");
			this.add(this.mResetVelYBox, "growx, wrap 20");
			
			this.add(new JLabel("Bond Stiffness: "), "span, split");
			this.mBondStiffnessField.setText(Double.toString(this.mEntity.getBondStiffness()));
			this.add(this.mBondStiffnessField, "growx, wrap 20");
			
			this.add(new JLabel("Graviational Force:"), "span");
			this.add(new JLabel("This is set on a per entity basis so"), "span");
			this.add(new JLabel("you can see the affect of gravity "), "span");
			this.add(new JLabel("on bounce height/deformation, etc "), "span");
			this.add(new JLabel ("X: "), "split");
			this.add(this.mGravityXBox, "growx, split");
			this.add(new JLabel ("Y: "), "split");
			this.add(this.mGravityYBox, "growx, wrap 20");
			this.mGravityXBox.setText(Double.toString(this.mEntity.getGravity().getX()));
			this.mGravityYBox.setText(Double.toString(this.mEntity.getGravity().getY()));
			
			this.add(new JLabel("Energy Loss Percent: "), "split");
			this.mEnergyLossBox.setText(Double.toString(this.mEntity.getEnergyLossPercentage()));
			this.add(this.mEnergyLossBox, "growx, wrap");
			
			this.add(new JLabel("Change Entity Colour:"), "span");
			this.add(this.mColBlueButton, "growx, span, split");
			this.add(this.mColGrayButton, "growx, span, split");
			this.add(this.mColGreenButton, "growx, wrap");
			this.add(this.mColPinkButton, "growx, span, split");
			this.add(this.mColRedButton, "growx, span, split");
			this.add(this.mColYellowButton, "growx, wrap 20");
			
			
			
			
		}
		this.invalidate();
		this.revalidate();
		this.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == this.mDeleteEntityButton && mEntity != null){

				this.mEntity.deleteEntity();
				this.mEntity = null;
				this.createPanel();
				
			
		} else if (event.getSource() == this.mBondStiffnessField && mEntity != null){
				double newVal = 1;
				
				try{
					newVal = Double.parseDouble(this.mBondStiffnessField.getText());
				} catch (NumberFormatException e){
					//show error dialog
					JOptionPane.showMessageDialog(this, "Must be a number!");
					//reset text in box
					this.mBondStiffnessField.setText(Double.toString(mEntity.getBondStiffness()));
					return;
				}
				
				this.mEntity.setBondStiffness(newVal);
				
			
		} else if (event.getSource() == this.mResetVelYBox || event.getSource() == this.mResetVelXBox && mEntity != null){
			double newX = 0;
			double newY = 0;
			
			if(this.mResetVelXBox.getText().isEmpty() && this.mResetVelYBox.getText().isEmpty()){
				return;
			}
			
			if(this.mResetVelXBox.getText().isEmpty()){
				this.mResetVelXBox.setText("0");
			}
			
			if(this.mResetVelYBox.getText().isEmpty()){
				this.mResetVelYBox.setText("0");
			}
			
			
			try{
				newX = Double.parseDouble(this.mResetVelXBox.getText());
				newY = Double.parseDouble(this.mResetVelYBox.getText());
			} catch (NumberFormatException e){
				//show error dialog
				JOptionPane.showMessageDialog(this, "Must enter a number!");
				return;
			}
			this.mEntity.setGlobalVelocity(new Vector2d(newX, newY));
		} else if (	event.getSource() == this.mColBlueButton || event.getSource() == this.mColGrayButton ||
					event.getSource() == this.mColGreenButton|| event.getSource() == this.mColPinkButton ||
					event.getSource() == this.mColRedButton || event.getSource() == this.mColYellowButton){
			//if it is one of the colour changing buttons, then set entities colour to the buttons colour
			this.mEntity.setColor(((Component) event.getSource()).getBackground());
		} else if (event.getSource() == this.mGravityXBox || event.getSource() == this.mGravityYBox){
			double gravX;
			double gravY;
			try{
				gravX = Double.parseDouble(this.mGravityXBox.getText());
				gravY = Double.parseDouble(this.mGravityYBox.getText());
			} catch (NumberFormatException e){
				//show error dialog
				JOptionPane.showMessageDialog(this, "Must enter a number!");
				return;
			}
			this.mEntity.setGravity(new Vector2d(gravX, gravY));
		} else if (event.getSource() == this.mEnergyLossBox){
			double loss;
			try{
				loss = Double.parseDouble(this.mEnergyLossBox.getText());
			} catch (NumberFormatException e){
				this.mEnergyLossBox.setText(Double.toString(this.mEntity.getEnergyLossPercentage()));
				JOptionPane.showMessageDialog(this, "Must enter a number!");
				return;
			}
			
			if(this.mEntity.setEnergyLosePercentage(loss)){
				return;
			} else {
				this.mEnergyLossBox.setText(Double.toString(this.mEntity.getEnergyLossPercentage()));
				JOptionPane.showMessageDialog(this, "Must be a number between 0 and 100!");
				return;
			}
		}
	}

	
}
