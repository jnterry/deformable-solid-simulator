package com.jamieterry.ballbounce;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.Border;

import net.miginfocom.swing.MigLayout;

import com.jamieterry.graphics.PanelRenderer;
import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.DeformableMeshEntity;
import com.jamieterry.simulation.EntityScene;

public class PanelSceneManagment extends JPanel implements ActionListener{
	
	JButton mClearSceneButton;
	JButton mAddBallButton;
	JButton mAddRectButton;
	EntityScene mScene;
	PanelRenderer mRenderPane;
	JTextField mSpeedBox;
	JCheckBox mDrawParticleVelocityCheck;
	JCheckBox mDrawBondsCheck;
	JCheckBox mDrawDarkLowVelPartsCheck;
	JCheckBox mDrawDebugInfoCheck;
	PanelDMeshEditor mDMeshEditor;
	
	
	
	PanelSceneManagment(EntityScene scene, PanelRenderer renderer){
		mScene = scene;
		mRenderPane = renderer;
		
		this.setLayout(new MigLayout());
		this.setMaximumSize(new Dimension(250,100000000));
		this.setMinimumSize(new Dimension(250,5000));
		this.setPreferredSize(new Dimension(250,768));
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		{
		JLabel title = new JLabel("Scene Options:");
		title.setFont(new Font("Arial", Font.BOLD, 20));
		this.add(title, "span");
		}
		
		this.mClearSceneButton = new JButton("Clear Scene");
		this.mClearSceneButton.addActionListener(this);
		this.add(this.mClearSceneButton, "span, growx");
		
		this.mAddBallButton = new JButton("Add Ball");
		this.mAddBallButton.addActionListener(this);
		this.add(this.mAddBallButton, "span, split 2, growx");

		this.mAddRectButton = new JButton("Add Rect");
		this.mAddRectButton.addActionListener(this);
		this.add(this.mAddRectButton, "growx, wrap");
		
		
		this.add(new JLabel("Speed Modifier:"), "span");
		this.mSpeedBox = new JTextField("1");
		this.mSpeedBox.setMaximumSize(new Dimension(250, 22));
		this.mSpeedBox.addActionListener(this);
		this.add(mSpeedBox, "growx, span");
		this.add(new JLabel("The higher the speed the"), "span");
		this.add(new JLabel("lower the accuracy!"), "span");
		
		this.mDrawParticleVelocityCheck = new JCheckBox("Draw Particle Velocity");
		this.mDrawParticleVelocityCheck.addActionListener(this);
		this.add(this.mDrawParticleVelocityCheck, "span");
		
		this.mDrawBondsCheck = new JCheckBox("Draw Bonds");
		this.mDrawBondsCheck.addActionListener(this);
		this.add(this.mDrawBondsCheck, "span");

		mDrawDarkLowVelPartsCheck = new JCheckBox("Darken Low Velocity Particles");
		mDrawDarkLowVelPartsCheck.addActionListener(this);
		this.add(this.mDrawDarkLowVelPartsCheck, "span");
		
		mDrawDebugInfoCheck = new JCheckBox("Draw Debug Info");
		mDrawDebugInfoCheck.addActionListener(this);
		this.add(this.mDrawDebugInfoCheck, "span");
		
		this.add(Box.createVerticalStrut(16));
		{
			JLabel title = new JLabel("Entity Options:");
			title.setFont(new Font("Arial", Font.BOLD, 20));
			this.add(title, "span");
			this.add(new JLabel("Click an entity to edit its properties"), "span");
			this.add(new JLabel("Once selected, you can move it"), "span");
			this.add(new JLabel("by clicking and dragging."), "span");
			this.add(Box.createVerticalStrut(8));
			this.mDMeshEditor = new PanelDMeshEditor();
			this.add(this.mDMeshEditor);
		}
		
	}
	
	
	public void setEditedEntity(DeformableMeshEntity e){
		this.mScene.setActiveEntity(e);
		this.mDMeshEditor.setEntity(e);
		this.mDMeshEditor.createPanel();
		//this.invalidate();
		this.mDMeshEditor.revalidate();
		this.mDMeshEditor.repaint();
		this.revalidate();
		this.repaint();
		//this.mDMeshEditor.invalidate();
	}


	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == this.mClearSceneButton){
			if(JOptionPane.showConfirmDialog(this, "Are you sure you wish to clear the scene? This cannot be undone!") == 0){
				//then user clicked yes, so clear the scene
				mScene.clearScene();
				
				//set selected entity to null, otherwise a now "deleted" entity will still be the selected one
				//(in reality, java's gc wont delete it as it being selected keeps a pointer to it, therefore
				//removing this pointer by setting it to null will actually allow the gc to free the entity's memory).
				mScene.setActiveEntity(null); 
				this.setEditedEntity(null); 
			}
		} else if (event.getSource() == this.mAddBallButton){
			try{
				int numRings = Integer.parseInt(JOptionPane.showInputDialog("Enter Number of Rings of Ball"));
				mScene.addEntity(new DeformableBall(new Vector2d(500,150),6,numRings,3,2));
			} catch (Exception e){
				JOptionPane.showMessageDialog(null, "Invalid input!");
				e.printStackTrace();
			}
			
		} else if (event.getSource() == this.mAddRectButton){
			try{
				int rows = Integer.parseInt(JOptionPane.showInputDialog("Enter Number of Rows"));
				int cols = Integer.parseInt(JOptionPane.showInputDialog("Enter Number of Columns"));
				mScene.addEntity(new DeformableRectangle(new Vector2d(500,150), rows, cols, 6, 4));
			} catch (Exception e){
				JOptionPane.showMessageDialog(null, "Invalid input!");
			}
			
		} else if (event.getSource() == this.mSpeedBox){
			double d;
			try{
				d = Double.parseDouble(this.mSpeedBox.getText());
			} catch (NumberFormatException e){
				JOptionPane.showMessageDialog(this, "Must be a number!");
				return;
			}
			this.mScene.setDeltaTimeModifier(d);
			
		} else if (event.getSource() == this.mDrawParticleVelocityCheck){
			DeformableMeshEntity.setDrawParticleVelocity(this.mDrawParticleVelocityCheck.isSelected());
		} else if (event.getSource() == this.mDrawBondsCheck){
			DeformableMeshEntity.setDrawBonds(this.mDrawBondsCheck.isSelected());
		} else if (event.getSource() == this.mDrawDarkLowVelPartsCheck){
			DeformableMeshEntity.setDarkenLowVelParticles(this.mDrawDarkLowVelPartsCheck.isSelected());
		} else if (event.getSource() == this.mDrawDebugInfoCheck){
			this.mScene.setDrawDebugInfo(this.mDrawDebugInfoCheck.isSelected());
			this.mRenderPane.setDisplayFramerate(this.mDrawDebugInfoCheck.isSelected());
		}
	}
}
