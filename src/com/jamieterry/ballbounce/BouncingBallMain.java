package com.jamieterry.ballbounce;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import com.jamieterry.graphics.PanelRenderer;
import com.jamieterry.math.Vector2d;
import com.jamieterry.simulation.DeformableMeshEntity;
import com.jamieterry.simulation.Entity;
import com.jamieterry.simulation.EntityScene;

public class BouncingBallMain implements ComponentListener{
	
	static EntityScene entScene;
	static PanelRenderer renderPane;
	static PanelSceneManagment sceneManager;
	 public static void main(String[] args) {
		JFrame frame = new JFrame("Deformable Ball Simulator - Jamie Terry");
		frame.setSize(1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
		entScene = new EntityScene();
		
		
		renderPane = new PanelRenderer(entScene);
		frame.getContentPane().add(renderPane);
		renderPane.addComponentListener(new BouncingBallMain());
		renderPane.addMouseListener(new renderMouseListener());
		renderPane.addMouseMotionListener(new renderMouseListener());
		
		sceneManager = new PanelSceneManagment(entScene, renderPane);
		frame.add(sceneManager);
		
		DeformableRectangle r1 = new DeformableRectangle(new Vector2d(50,175), 10, 10, 6, 4);
		entScene.addEntity(r1);
		r1.setGlobalVelocity(new Vector2d(12000,0));
		
		DeformableBall b1 = new DeformableBall(new Vector2d(300,250),6,3,3,2);
		b1.setGlobalVelocity(new Vector2d(0,0));
		entScene.addEntity(b1);
		b1.setBondStiffness(7);
		
		
		entScene.setDeltaTimeModifier(1);
		
		frame.setVisible(true);
		entScene.beginUpdateThread();
		
		while(frame.isVisible()){
			renderPane.repaint();
			entScene.setSize(renderPane.getSize().width, renderPane.getSize().height);
			

			try {
				Thread.sleep(16);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Program exited");
		System.exit(0);
	 
	 }

	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent arg0) {
		entScene.setSize(renderPane.getSize().width, renderPane.getSize().height);
	}

	@Override
	public void componentShown(ComponentEvent arg0) {}
	
	static class renderMouseListener implements MouseListener, MouseMotionListener{
		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			//get entity at mouse position
			Entity ent = entScene.getEntityAtPoint(new Vector2d(e.getPoint().x, e.getPoint().y));
			
			//set the scenes active entity, and the scene managers active entity
			entScene.setActiveEntity(ent);
			
			if(ent instanceof DeformableMeshEntity || ent == null){
				sceneManager.setEditedEntity((DeformableMeshEntity)ent);
			}
			
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			Vector2d mousePos = new Vector2d(e.getPoint().x, e.getPoint().y);
			Entity ent = entScene.getEntityAtPoint(mousePos);
			if(ent == entScene.getActiveEntity() && ent != null){
				ent.setPosition(mousePos);
				if(ent instanceof DeformableMeshEntity){
					((DeformableMeshEntity)ent).setGlobalVelocity(new Vector2d());
				}
			}
			
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			
			
			
		}
		
	}

}
